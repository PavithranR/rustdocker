FROM rust:alpine as builder
WORKDIR /app/RustDocker/
COPY . .
RUN cargo build --release

FROM scratch
COPY --from=builder /app/RustDocker/target/release/RustDocker /app/RustDocker
CMD ["/app/RustDocker"]
